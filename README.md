# binary-decoder 1.0.0

Decoder for structure binary data

[![pipeline status](https://gitlab.com/bytesnz/binary-decoder/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/binary-decoder/commits/main)
[![binary-decoder on NPM](https://bytes.nz/b/binary-decoder/npm)](https://npmjs.com/package/binary-decoder)
[![license](https://bytes.nz/b/binary-decoder/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/binary-decoder/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/binary-decoder/custom?color=yellowgreen&name=development+time&value=~5+hours)](https://gitlab.com/bytesnz/binary-decoder/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/binary-decoder/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/binary-decoder/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/binary-decoder/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Decoder module for decoding binary data that has a structured but variable
structure. The structure of the data is specified using an array of data parts.

## Data Types

### `byte`, `uint8`, `int8`, `uint16`, `int16`, `uint32`, `int32`, `uint63`, `int64`

An integer

#### Options

- **id** ID of field (will be used as the key in the output object)
- **littleEndian** If true, will decode as littleEndian
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed.
- **useBigInt** Always return a BigInt instead of a Number for integers. By
  default, if the number is 4 bytes or smaller, the value will be stored as
  a Number.

### `bytes`

An custom byte-length integer

#### Options

- **id** ID of field (will be used as the key in the output object)
- **littleEndian** If true, will decode as littleEndian
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed.
- **useBigInt** Always return a BigInt instead of a Number. By default, if
  the number is 4 bytes or smaller, the value will be stored as a Number.

### `bits`

Bit-size structured data. Must total a number of bytes.

#### Options

- **id** ID of field (will be used as the key in the output object). If not
  given, bit values will be set in containing object.
- **parts** Array of bit parts that make up the data
  - **id** ID of the bit part
  - **bit** The number of bits in the bit part
  - **enums** Enum of the bit value
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed. This is useful for using a map based
  on the first x-bits of a byte when the map includes the remaining bits in
  the byte

### `bitsInt`

Integer spread across multiple, separated bits

#### Options

- **id** ID of field (will be used as the key in the output object)
- **bits** An array of bits to skip/take in an alternating fashion e.g.
  [0, 5, 3, 5, 3, 8] will be an 16-bit integer from 3 bytes, bits8:4 of the
  first byte, bits 8:4 of the second and the full last byte
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed. This is useful for using a map based
  on the first x-bits of a byte when the map includes the remaining bits in
  the byte
- **useBigInt** Always return a BigInt instead of a Number. By default, if
  the number is 4 bytes or smaller, the value will be stored as a Number.

### `variable`

Variable-length number

#### Options

- **id** ID of field (will be used as the key in the output object)
- **length** Length or key to value of length of data

### `string`

String. If a NULL character (`\0`) is encountered, the string will be
terminated and the rest of the bytes will be discarded unless the `noDiscard`
option is set.

#### Options

- **id** ID of field (will be used as the key in the output object)
- **length** Length or key to value of length of string

### `repeat`

n-parts of structured data

#### Options

- **id** ID of field (will be used as the key in the output object)
- **repeats** Number of parts
- **asArray** Store parts in an array
- **asObject** Store the parts in an object using the value for the given key
  as the part's key in the object

### `map`

Data that can be any of a given number of structures

#### Options

- **id** ID of field (will be used as the key in the output object)
- **key** Key to value to determine structure of data
- **structures** Object of value/structure of the possible data structures

## Example

```js
import { readFile } from 'fs/promises';
import { createDecoder } from 'binary-decoder';
import structure from './structure.js';

const decoder = createDecoder(structure, {
  littleEndian: false,
  throwOnInvalidEnumValue: true,
  logger: {
    debug: (...args) => console.debug(...args)
  }
});

const { nextByte, decodedMessage } = decoder.decode(new Uint8Array(await readFile('data.bin')));

console.log(decodedMessage);

```

### _structure.js_

```js
/** @type {import('binary-decoder').MessageStructure} */
export default [
  {
    type: 'uint16',
    id: 'messageLength'
  },
  {
    type: 'repeat',
    id: 'data',
    asObject: 'type',
    structure: [
      {
        type: 'byte',
        id: 'type',
        enums: [
          {
            value: 1,
            label: 'Header',
            key: 'header'
          },
          {
            value: 2,
            label: 'Payload',
            key: 'payload'
          }
        ]
      },
      {
        type: 'map',
        key: 'type',
        structures: {
          1: [
            {
              type: 'uint8',
              id: 'idLength'
            },
            {
              type: 'string',
              id: 'id',
              length: 'idLength'
            }
          ],
          2: [
            {
              type: 'uint16',
              id: 'payloadLength'
            },
            {
              type: 'variable',
              id: 'payload',
              length: 'payloadLength'
            }
          ]
        }
      }
    ]
  }
];

```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/binary-decoder/issues) or
[email](mailto:contact-project+bytesnz-binary-decoder-37036554-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/binary-decoder/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](git+https://gitlab.com/bytesnz/binary-decoder.git) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2024-05-19

**Breaking Change** Now uses [BigInt][] (ES2020) so older browsers and versions
of Node (&lt;10.4.0) are not supported.

### Added

- Ability to integers larger than 32 bits
- `useBigInt` option to always return [BigInt][]s

## [0.6.2] - 2024-05-14

### Fixed

- Fixed handling of 4 byte integers

## [0.6.1] - 2023-03-04

### Fixed

- Log level of initial log message

## [0.6.0] - 2022-12-21

### Changed

- Allowed `bits` values to be stored in an object using the `id` parameter

## [0.5.0] - 2022-08-05

### Changed

- If a NULL character `\0` is encountered in a string, the string will be
  terminated and the rest of the bytes of the string will be discarded unless
  the `noDiscard` option is set

## [0.4.0] - 2022-07-25

### Added

- `bitsInt` for integers spread across multiple bytes with bits that should
  be ignored

## [0.3.0] - 2022-06-16

### Fixed

- **Breaking** Corrects mapping of enums for integer types
  (`byte`, `uint8` etc).

  This will break functionality if you were using the incorrect key of `enum`
  (instead of `enums`) for the enums array. `enums` is used instead of `enum`
  as `enum` is a reserved word in JavaScript/ECMAScript.

## [0.2.0] - 2022-06-16

### Added

- `partial` flag to `bits` type adding the ability to use a `map` from an
  key value smaller than a byte
- Handling of multi-byte bit ranges
- Throw error on not enough data

## [0.1.0] - 2022-06-15

Initial version!

[bigint]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/BigInt
[1.0.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.2...v1.0.0
[0.6.2]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/binary-decoder/tree/v0.1.0


[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
