# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2024-05-19

**Breaking Change** Now uses [BigInt][] (ES2020) so older browsers and versions
of Node (<10.4.0) are not supported.

### Added

- Ability to integers larger than 32 bits
- `useBigInt` option to always return [BigInt][]s

## [0.6.2] - 2024-05-14

### Fixed

- Fixed handling of 4 byte integers

## [0.6.1] - 2023-03-04

### Fixed

- Log level of initial log message

## [0.6.0] - 2022-12-21

### Changed

- Allowed `bits` values to be stored in an object using the `id` parameter

## [0.5.0] - 2022-08-05

### Changed

- If a NULL character `\0` is encountered in a string, the string will be
  terminated and the rest of the bytes of the string will be discarded unless
  the `noDiscard` option is set

## [0.4.0] - 2022-07-25

### Added

- `bitsInt` for integers spread across multiple bytes with bits that should
  be ignored

## [0.3.0] - 2022-06-16

### Fixed

- **Breaking** Corrects mapping of enums for integer types
  (`byte`, `uint8` etc).

  This will break functionality if you were using the incorrect key of `enum`
  (instead of `enums`) for the enums array. `enums` is used instead of `enum`
  as `enum` is a reserved word in JavaScript/ECMAScript.

## [0.2.0] - 2022-06-16

### Added

- `partial` flag to `bits` type adding the ability to use a `map` from an
  key value smaller than a byte
- Handling of multi-byte bit ranges
- Throw error on not enough data

## [0.1.0] - 2022-06-15

Initial version!

[bigint]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/BigInt
[1.0.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.2...v1.0.0
[0.6.2]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/bytesnz/binary-decoder/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/binary-decoder/tree/v0.1.0
