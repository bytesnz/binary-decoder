/**
 * Create a binary decoder for the messages given
 *
 * @param {MessageStructure} messages Expected messages
 * @param {Options} options Decoder options
 *
 * @returns Decoder The decoder
 */
export const createDecoder = (structure, options) => {
	options = {
		...(options || {})
	};

	///TODO Add validation for structure

	return {
		/**
		 * Decode a binary message
		 *
		 * @param {Uint8Array} message Message to decode
		 * @param {number} [length] The maximum number of bytes to consume. If
		 *   not set, will use message length
		 */
		decode: (message, length) => {
			options.logger?.debug?.('decoding', message);

			return decodeStructure(structure, message, options, length);
		}
	};
};

/** @typedef {Object} ValueEnum
 *
 * @property {any} value Value for enum
 * @property {string} label Label for enum value
 * @property {string} key Key to use for enum value
 */

/** @typedef {Object} BaseDataType
 *
 * @property {string} type Type of data
 * @property {string} id Key of data
 * @property {ValueEnum} enums Enum for data value
 * @property {boolean} [throwOnInvalidEnumValue] If true, an Error will be
 *   thrown if the value doesn't match any of the enum values
 */

/** @typedef {Object} IntegerDataAdditional
 * Standard length integer
 *
 * @property {'byte'|'uint8'|'int8'|'uint16'|'int16'|'uint32'|'int32'|'uint64'|'int64'} type
 * @property {boolean} [littleEndian] If true, integer will be decoded as
 *   littleEndian, otherwise bigEndian/networkByteOrder will be used
 *
 * @typedef {BaseDataType & IntegerDataAdditional} IntegerDataType
 */

/**  @typedef {Object} BytesDataAdditional
 * Custom length integer
 *
 * @property {number} bytes Number of bytes
 * @property {boolean} [partial] If true, the nextByte counter won't be
 *   incremented after decoding the values in this segment, meaning they
 *   will be processed by the next segment.
 *
 * @typedef {BaseDataType & BytesDataAdditional} BytesDataType
 */

/** @typedef {Object} BitPart
 *
 * @property {string} id ID for bit data. If not given, bits will be ignored
 * @property {number} bits Number of bits
 * @property {ValueEnum} enums Enum for data value
 */

/** @typedef {Object} BitsDataAdditional
 * Bit-sized binary data
 *
 * @property {'bits'} type
 * @property {BitPart} parts Bit-size binary data parts. Should total a multiple of
 *  bytes
 * @property {boolean} [partial] If true, the nextByte counter won't be
 *   incremented after decoding the values in this segment, meaning they
 *   will be processed by the next segment.
 *
 * @typedef {BaseDataType & BitsDataAdditional} BitsDataType
 */

/** @typedef {Object} BitsIntAdditional
 * Integer spread over multiple parts
 *
 * @property {Array<number>} bits Bits to skip/take in alternating fashion.
 *   e.g. [0, 5, 3, 5, 3, 8] will be an 16-bit integer from 3 bytes,
 *   bits8:4 of the first byte, bits 8:4 of the second and the full last byte
 * @property {boolean} [partial] If true, the nextByte counter won't be
 *   incremented after decoding the values in this segment, meaning they
 *   will be processed by the next segment.
 *
 * @typedef {BaseDataType & BitsIntAdditional} BitsIntType

/** @typedef {Object} VariableDataAdditional
 * Variable length data. Will be stored as an Uint8Array
 *
 * @property {'variable'} type
 * @property {number|string} bytes Number of bytes of data or key
 *   to get the number of bytes from
 *
 * @typedef {BaseDataType & VariableDataAdditional} VariableDataType
 */

/** @typedef {Object} StringDataAdditional
 * String data
 *
 * @property {'string'} type
 * @property {number|string} length The length of the string or the key
 *   to get the length of the string from
 * @property {boolean} [noDiscard] If true, a NULL character (\0) will not
 *   terminate the string. Otherwise the string will be terminated and the
 *   rest of the string characters will be discarded
 *
 * @typedef {BaseDataType & StringDataAdditional} StringDataType
 */

/** @typedef {Object} RepeatDataType
 * Repeated data
 *
 * @property {'repeat'} type
 * @property {number|string} repeats Number of repeats or the key to get the
 *   number of repeats from
 * @property {boolean} [asArray] If true, repeats will be stored in an array
 * @property {string} [asObject] If set, repeats will be stored in an object
 *  with the key for each repeat being the value of the key given in asObject
 * @property {MessageStructure} structure Structure of the repeated data
 */

/** @typedef {Object} MapDataType
 * Variable data type, using a previous value to determine the structure of
 * the data
 *
 * @property {'map'} type
 * @property {string} key Key to get map value from
 * @property {{[key: string]: MessageStructure} structures Map of data
 *   structures to use for data
 */

/** @typedef {Array<IntegerDataType|BytesDataType|BitsDataType|BitsIntType|VariableDataType|StringDataType|RepeatDataType|MapDataType>} MessageStructure
 *
 * Structure configuration for the expected messages
 */

/** @typedef {(...args) => void} LoggerFn */

/** @typedef {Object} Logger
 *
 * @property {LoggerFn} [log]
 * @property {LoggerFn} [warn]
 * @property {LoggerFn} [error]
 * @property {LoggerFn} [info]
 * @property {LoggerFn} [debug]
 */

/** @typedef {Object} Options
 * Decoder options
 *
 * @property {boolean} [littleEndian] If true, integers will be decoded as
 *   littleEndian, otherwise bigEndian/networkByteOrder will be used
 * @property {boolean} [throwOnInvalidEnumValue] If true, an Error will be
 *   thrown if the value doesn't match any of the enum values
 * @property {Logger} [logger] Functions to use to log message during decoding
 */

/**
 * Decode the given binary message using the given MessageStructure
 *
 * @param {MessageStructure} structure Structure of binary message
 * @param {Uint8Array} message Binary message to decode
 * @param {Options} option Decode options
 * @param {number} [nextByte] Index to start at for decode. If not given,
 *   will start at the first byte
 * @param {Array<object>} [parentDecodedMessages] Parent decoded messages
 */
const decodeStructure = (
	structure,
	message,
	options,
	messageLength,
	nextByte,
	parentDecodedMessages
) => {
	const decodedMessage = {};

	if (!parentDecodedMessages) {
		parentDecodedMessages = [];
	}

	if (!nextByte) {
		nextByte = 0;
	}

	for (const part of structure) {
		options.logger?.debug?.('Decoding part', part.id, part, 'at byte', nextByte);
		let bytes = null;
		let signed = false;

		switch (part.type) {
			case 'int8':
			case 'int16':
			case 'int32':
			case 'int64':
				signed = true;
			// eslint-disable-line no-fallthrough
			case 'byte':
			case 'uint8':
			case 'uint16':
			case 'uint32':
			case 'uint64':
				switch (part.type) {
					case 'uint8':
					case 'int8':
					case 'byte':
						bytes = 1;
						break;
					case 'int16':
					case 'uint16':
						bytes = 2;
						break;
					case 'int32':
					case 'uint32':
						bytes = 4;
						break;
					case 'int64':
					case 'uint64':
						bytes = 8;
						break;
				}
			// eslint-disable-line no-fallthrough
			case 'bytes': {
				let negative = false;

				let value = BigInt(0);

				if (bytes === null) {
					bytes = part.bytes;
				}

				if (nextByte + bytes > message.length) {
					throw new Error('Not enough data for structure');
				}

				if (signed) {
					const highestBit =
						options.littleEndian || part.littleEndian ? nextByte + (bytes - 1) : nextByte;

					if (message[highestBit] >= 128) {
						negative = true;
					}
				}

				if (options.littleEndian || part.littleEndian) {
					for (let i = 0; i < bytes; i++) {
						value += message[nextByte + i] << (i * 8);
					}
				} else {
					for (let i = 0; i < bytes; i++) {
						value += BigInt(message[nextByte + i]) << BigInt((bytes - 1 - i) * 8);
					}
				}

				if (negative) {
					value -= BigInt(Math.pow(2, bytes * 8));
				}

				if (!(bytes > 4 || options.useBigInt || structure.useBigInt)) {
					value = Number(value);
				}

				if (part.enums) {
					value = findEnum(part.enums, value, part, options);
				}

				options.logger?.debug?.('Adding value', part.id, value);
				decodedMessage[part.id] = value;

				if (!part.partial) {
					nextByte += bytes;
				}
				break;
			}
			case 'bitsInt': {
				let value = BigInt(0);
				let bit = 0;
				let b = 0;
				let totalBits = 0;
				options.logger?.debug?.('Have bitsInt', part);

				for (let i = 0; i < part.bits.length; i++) {
					if (i % 2 === 0) {
						// Skip even amounts
						options.logger?.debug?.('Skipping', part.bits[i], 'bits');
						bit += part.bits[i];
						continue;
					}

					let left = part.bits[i];
					totalBits += left;
					value = value << BigInt(left);
					while (left) {
						b += Math.floor(bit / 8);
						bit = bit % 8;

						const current = Math.min(8 - bit, left);
						const mask = (0xff >> (8 - current)) << (8 - bit - current);
						const currentValue = (message[nextByte + b] & mask) >> (8 - bit - current);
						options.logger?.debug?.('adding partial value', '0x' + currentValue.toString(16));
						value += BigInt(currentValue << (left - current));
						left -= current;
						bit += current;
					}
				}

				if (part.enums) {
					value = findEnum(part.enums, Number(value), part, options);
				}

				if (!(totalBits > 4 * 8 || options.useBigInt || structure.useBigInt)) {
					value = Number(value);
				}

				options.logger?.debug?.('Adding value', part.id, value);
				decodedMessage[part.id] = value;

				if (!part.partial) {
					nextByte += b + Math.ceil(bit / 8);
				}

				break;
			}
			case 'bits': {
				let bit = 0;
				let b = 0;
				let store;

				if (part.id) {
					store = {};
					decodedMessage[part.id] = store;
				} else {
					store = decodedMessage;
				}

				// TODO Handle multiple byte bits
				// TODO Handle bits over multiple bytes
				for (const bits of part.parts) {
					b += Math.floor(bit / 8);
					bit = bit % 8;
					if (nextByte + b + Math.ceil((bit + bits.bits) / 8) > message.length) {
						throw new Error('Not enough data for structure');
					}
					if (!bits.id) {
						bit += bits.bits;
						continue;
					}
					if (bits.bits <= 8 - bit) {
						// Part contained in a single byte
						const mask = (0xff >> (8 - bits.bits)) << (8 - bit - bits.bits);
						let value = (message[nextByte + b] & mask) >> (8 - bit - bits.bits);

						if (bits.enums) {
							value = findEnum(bits.enums, value, bits, options);
						}
						options.logger?.debug?.('Adding value', bits.id, value);
						store[bits.id] = value;
						bit += bits.bits;
					} else {
						// Multi-byte part
						let value = 0;
						let left = bits.bits;

						while (left) {
							b += Math.floor(bit / 8);
							bit = bit % 8;
							const current = Math.min(8 - bit, left);

							const mask = (0xff >> (8 - current)) << (8 - bit - current);
							const currentValue = (message[nextByte + b] & mask) >> (8 - bit - current);
							value += currentValue << (left - current);
							left -= current;
							bit += current;
						}

						if (bits.enums) {
							value = findEnum(bits.enums, value, bits, options);
						}
						options.logger?.debug?.('Adding value', bits.id, value);
						store[bits.id] = value;
					}
				}
				if (!part.partial) {
					nextByte += b + Math.ceil(bit / 8);
				}
				break;
			}
			case 'variable': {
				let bytes;
				if (typeof part.length === 'number') {
					bytes = part.length;
				} else {
					bytes = findValue(part.length, decodedMessage, parentDecodedMessages);
				}

				if (typeof bytes !== 'number') {
					throw new Error(`Invalid value for number of bytes ${part.length} for ${part.id}`);
				}

				options.logger?.debug?.('Variable length is', bytes);

				if (bytes) {
					if (nextByte + bytes > message.length) {
						throw new Error('Not enough data for structure');
					}
					switch (part.format) {
						case 'raw':
						default:
							decodedMessage[part.id] = message.slice(nextByte, nextByte + bytes);
							break;
					}

					nextByte += bytes;
				}
				break;
			}
			case 'string': {
				let value = '';

				let length;

				if (typeof part.length === 'string') {
					length = decodedMessage[part.length];
				} else {
					length = part.length;
				}

				if (typeof length !== 'number') {
					throw new Error(`Need a valid length for string ${part.length} for ${part.id}`);
				}

				if (nextByte + length > message.length) {
					throw new Error('Not enough data for structure');
				}

				let terminated = false;
				for (let i = 0; i < length; i++) {
					if (!part.noDiscard && message[nextByte + i] === 0) {
						terminated = true;
					}
					if (!terminated) {
						value += String.fromCharCode(message[nextByte + i]);
					}
				}

				if (part.enums) {
					value = findEnum(part.enums, value, part, options);
				}

				options.logger?.debug?.('Adding value', part.id, value);
				decodedMessage[part.id] = value;

				nextByte += length;
				break;
			}
			case 'repeat':
				if (part.asArray) {
					decodedMessage[part.id] = [];
				} else if (part.asObject) {
					decodedMessage[part.id] = {};
				}

				if (part.repeats) {
					let repeats;
					if (typeof part.repeats === 'string') {
						repeats = findValue(part.repeats, decodedMessage, parentDecodedMessages);
						if (typeof repeats !== 'number') {
							throw new Error(`Could not find repeats value ${part.repeats} for ${part.id}`);
						}
					} else {
						repeats = part.repeats;
					}

					for (let i = 0; i < repeats; i++) {
						const result = decodeStructure(
							part.structure,
							message,
							options,
							messageLength,
							nextByte,
							[...parentDecodedMessages, decodedMessage]
						);
						nextByte = result.nextByte;
						if (part.asArray) {
							options.logger?.debug?.('Adding repeat value', result.decodedMessage);
							decodedMessage[part.id].push(result.decodedMessage);
						} else if (part.asObject) {
							const key =
								result.decodedMessage[part.asObject]?.key ?? result.decodedMessage[part.asObject];

							if (typeof key == 'undefined') {
								throw new Error(`No key value for repeat ${result.decodedMessage} for ${part.id}`);
							}

							options.logger?.debug?.('Adding repeat key/value', key, result.decodedMessage);
							decodedMessage[part.id][key] = result.decodedMessage;
						} else {
							options.logger?.debug?.('Merging repeat value', result.decodedMessage);
							Object.assign(decodedMessage, result.decodedMessage);
						}
					}
				} else {
					while (nextByte < message.length) {
						const result = decodeStructure(
							part.structure,
							message,
							options,
							messageLength,
							nextByte,
							[...parentDecodedMessages, decodedMessage]
						);
						nextByte = result.nextByte;
						if (part.asArray) {
							options.logger?.debug?.('Adding repeat value', result.decodedMessage);
							decodedMessage[part.id].push(result.decodedMessage);
						} else if (part.asObject) {
							const key =
								result.decodedMessage[part.asObject]?.key ?? result.decodedMessage[part.asObject];

							if (typeof key == 'undefined') {
								throw new Error(`No key value for repeat ${result.decodedMessage} for ${part.id}`);
							}

							options.logger?.debug?.('Adding repeat key/value', key, result.decodedMessage);
							decodedMessage[part.id][key] = result.decodedMessage;
						} else {
							options.logger?.debug?.('Merging repeat value', result.decodedMessage);
							Object.assign(decodedMessage, result.decodedMessage);
						}
					}
				}
				break;
			case 'map':
				const mapKey = findValue(part.key, decodedMessage, parentDecodedMessages);

				const mapStructure = part.structures[mapKey];

				if (!mapStructure) {
					throw new Error(`Unknown map key ${mapKey} for part ${part.id}`);
				}

				const result = decodeStructure(mapStructure, message, options, messageLength, nextByte, [
					...parentDecodedMessages,
					decodedMessage
				]);
				nextByte = result.nextByte;
				options.logger?.debug?.('Merging map value', result.decodedMessage);
				Object.assign(decodedMessage, result.decodedMessage);
				break;
		}
	}

	return {
		decodedMessage,
		nextByte
	};
};

const findValue = (key, decodedMessage, parentDecodedMessages) => {
	if (Object.prototype.hasOwnProperty.call(decodedMessage, key)) {
		return decodedMessage[key]?.value ?? decodedMessage[key];
	}

	for (let i = parentDecodedMessages.length - 1; i >= 0; i--) {
		if (Object.prototype.hasOwnProperty.call(parentDecodedMessages[i], key)) {
			return parentDecodedMessages[i][key]?.value ?? parentDecodedMessages[i][key];
		}
	}
};

const findEnum = (enums, value, part, options) => {
	for (let i = 0; i < enums.length; i++) {
		if (enums[i].value === value) {
			return enums[i];
		}
	}

	if (part.throwOnInvalidEnumValue ?? options.throwOnInvalidEnumValue) {
		throw new Error(`Invalid enum value for ${part.id} ${value}`);
	}

	return value;
};

/**
 * Convert a byte to ASCII hex
 *
 * @param {number} value Value to convert
 *
 * @returns Hex string
 */
const toHex = (value) => {
	let string = value.toString(16);

	if (string.length === 1) {
		return '0' + string;
	}

	return string;
};

const toHexString = (array, length, start) => {
	let string = '';
	if (!start) {
		start = 0;
	}

	for (let i = start; i < start + length; i++) {
		string += toHex(array[i]);

		if (i % 2 && i !== 0 && i != start + length - 1) {
			string += ' ';
		}
	}

	return string;
};
