export const bitsIntPartial = [
	{
		type: 'bitsInt',
		id: 'int',
		bits: [1, 7, 5, 3],
		partial: true
	}
];

export const bitsInt = [
	{
		type: 'bitsInt',
		id: 'int',
		bits: [1, 7, 5, 3]
	}
];

export const bitsIntBridge = [
	{
		type: 'bitsInt',
		id: 'int',
		bits: [4, 8, 4, 4]
	}
];
