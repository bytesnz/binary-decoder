import { assert } from 'chai';

import { createDecoder } from '../index.js';

import { readFile } from 'node:fs/promises';

import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

import typesStructure from './types.js';
import exampleStructure from './structure.js';
import bitsStructure from './bits.js';
import { onesFail, onesPass } from './ones.js';
import { bitsInt, bitsIntPartial, bitsIntBridge } from './bitsInt.js';
import { stringDiscard, stringNoDiscard } from './string.js';

let decoder = createDecoder(typesStructure);

assert.deepEqual(
	decoder.decode(new Uint8Array(await readFile(join(__dirname, 'types.bin')))),
	{
		decodedMessage: {
			smallBit: 247,
			bigBit: 60129476316n,
			zeroByte: 0,
			byte: 247,
			uint8: 254,
			int8: -2,
			uint16: 65244,
			int16: -292,
			uint32: 4275878552,
			int32: -19088744,
			uint64: 18364758544493064720n,
			int64: -81985529216486896n
		},
		nextByte: 32
	},
	'did not decode types.bin correctly'
);

decoder = createDecoder(exampleStructure);
assert.deepEqual(decoder.decode(new Uint8Array(await readFile(join(__dirname, 'data.bin')))), {
	decodedMessage: {
		messageLength: 16,
		data: {
			header: {
				type: { value: 1, label: 'Header', key: 'header' },
				idLength: 4,
				id: 'Test'
			},
			payload: {
				type: { value: 2, label: 'Payload', key: 'payload' },
				payloadLength: 5,
				payload: new Uint8Array([72, 101, 108, 108, 111])
			}
		}
	},
	nextByte: 16
});

decoder = createDecoder(bitsStructure);
assert.deepEqual(decoder.decode(new Uint8Array(await readFile(join(__dirname, 'bits.bin')))), {
	decodedMessage: {
		part1: 1,
		part2: 2,
		part3: 3,
		part4: 4,
		part5: 5,
		part6: 6,
		part7: 7,
		part8: 301,
		byte: 255
	},
	nextByte: 8
});

for (let structure of onesFail) {
	decoder = createDecoder(structure);

	try {
		decoder.decode(new Uint8Array([0xff]));
		assert.fail('Did not throw for ' + JSON.stringify(structure));
	} catch (error) {
		if (!error.message.startsWith('Not enough')) {
			console.error('caught failure for', structure);
			throw error;
		}
	}
}

for (let structure of onesPass) {
	decoder = createDecoder(structure);

	try {
		decoder.decode(new Uint8Array([0xff]));
	} catch (error) {
		console.error('Caught error for', structure);
		throw error;
	}
}

decoder = createDecoder(bitsInt);

assert.deepEqual(decoder.decode(new Uint8Array([0xaa, 0xfa])), {
	decodedMessage: {
		int: 338
	},
	nextByte: 2
});

decoder = createDecoder(bitsIntPartial);

assert.deepEqual(decoder.decode(new Uint8Array([0xaa, 0xfa])), {
	decodedMessage: {
		int: 338
	},
	nextByte: 0
});

decoder = createDecoder(bitsIntBridge);

assert.deepEqual(decoder.decode(new Uint8Array([0xf5, 0x5f, 0x5f])), {
	decodedMessage: {
		int: 1365
	},
	nextByte: 3
});

decoder = createDecoder(stringDiscard);

assert.deepEqual(decoder.decode(new Uint8Array([0x31, 0x32, 0x00, 0x34, 0x35])), {
	decodedMessage: {
		test: '12'
	},
	nextByte: 5
});

decoder = createDecoder(stringNoDiscard);

assert.deepEqual(decoder.decode(new Uint8Array([0x31, 0x32, 0x00, 0x34, 0x35])), {
	decodedMessage: {
		test: '12\x0045'
	},
	nextByte: 5
});
