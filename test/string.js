export const stringDiscard = [
	{
		type: 'string',
		id: 'test',
		length: 5
	}
];

export const stringNoDiscard = [
	{
		type: 'string',
		id: 'test',
		length: 5,
		noDiscard: true
	}
];
