export const onesFail = [
	[
		{
			type: 'uint16',
			id: 'fail'
		}
	],
	[
		{
			type: 'variable',
			length: 2,
			id: 'fail'
		}
	],
	[
		{
			type: 'string',
			length: 2,
			id: 'fail'
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 12
				}
			]
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 12,
					id: 'fail'
				}
			]
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 7
				},
				{
					bits: 2,
					id: 'fail'
				}
			]
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 8
				},
				{
					bits: 2,
					id: 'fail'
				}
			]
		}
	],
	[
		{
			type: 'repeat',
			repeats: 2,
			structure: [
				{
					type: 'byte',
					id: 'fail'
				}
			]
		}
	]
];

export const onesPass = [
	[
		{
			type: 'byte',
			id: 'pass'
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 8
				}
			]
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 8,
					id: 'pass'
				}
			]
		}
	],
	[
		{
			type: 'bits',
			parts: [
				{
					bits: 2
				},
				{
					bits: 6,
					id: 'pass'
				}
			]
		}
	],
	[
		{
			type: 'variable',
			length: 1,
			id: 'pass'
		}
	],
	[
		{
			type: 'string',
			length: 1,
			id: 'pass'
		}
	],
	[
		{
			type: 'repeat',
			repeats: 1,
			structure: [
				{
					type: 'byte',
					id: 'pass'
				}
			]
		}
	]
];
