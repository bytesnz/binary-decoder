import { readFile } from 'fs/promises';
import { createDecoder } from 'binary-decoder';
import structure from './structure.js';

const decoder = createDecoder(structure, {
	littleEndian: false,
	throwOnInvalidEnumValue: true,
	logger: {
		debug: (...args) => console.debug(...args)
	}
});

const { nextByte, decodedMessage } = decoder.decode(new Uint8Array(await readFile('data.bin')));

console.log(decodedMessage);
