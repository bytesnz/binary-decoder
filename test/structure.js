/** @type {import('binary-decoder').MessageStructure} */
export default [
	{
		type: 'uint16',
		id: 'messageLength'
	},
	{
		type: 'repeat',
		id: 'data',
		asObject: 'type',
		structure: [
			{
				type: 'byte',
				id: 'type',
				enums: [
					{
						value: 1,
						label: 'Header',
						key: 'header'
					},
					{
						value: 2,
						label: 'Payload',
						key: 'payload'
					}
				]
			},
			{
				type: 'map',
				key: 'type',
				structures: {
					1: [
						{
							type: 'uint8',
							id: 'idLength'
						},
						{
							type: 'string',
							id: 'id',
							length: 'idLength'
						}
					],
					2: [
						{
							type: 'uint16',
							id: 'payloadLength'
						},
						{
							type: 'variable',
							id: 'payload',
							length: 'payloadLength'
						}
					]
				}
			}
		]
	}
];
