/* @type {import(../index.d.ts).MessageStructure} */
export default [
	{
		id: 'smallBit',
		type: 'bitsInt',
		bits: [8, 8],
		partial: true
	},
	{
		id: 'bigBit',
		type: 'bitsInt',
		bits: [10, 4, 0, 8, 2, 24],
		partial: true
	},
	{
		type: 'byte',
		id: 'zeroByte'
	},
	{
		type: 'byte',
		id: 'byte'
	},
	{
		type: 'uint8',
		id: 'uint8'
	},
	{
		type: 'int8',
		id: 'int8'
	},
	{
		type: 'uint16',
		id: 'uint16'
	},
	{
		type: 'int16',
		id: 'int16'
	},
	{
		type: 'uint32',
		id: 'uint32'
	},
	{
		type: 'int32',
		id: 'int32'
	},
	{
		type: 'uint64',
		id: 'uint64'
	},
	{
		type: 'int64',
		id: 'int64'
	}
];
