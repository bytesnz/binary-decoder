export default [
	{
		type: 'bits',
		parts: [
			{
				bits: 3,
				id: 'part1'
			},
			{
				bits: 5,
				id: 'part2'
			},
			{
				bits: 5,
				id: 'part3'
			},
			{
				bits: 3,
				id: 'part4'
			}
		]
	},
	{
		type: 'bits',
		parts: [
			{
				bits: 3,
				id: 'part5'
			}
		],
		partial: true
	},
	{
		type: 'bits',
		parts: [
			{
				bits: 3
			},
			{
				bits: 8,
				id: 'part6'
			},
			{
				bits: 5,
				id: 'part7'
			},
			{
				bits: 20,
				id: 'part8'
			}
		]
	},
	{
		type: 'byte',
		id: 'byte'
	}
];
