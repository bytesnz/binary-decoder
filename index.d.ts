export function createDecoder(
	structure: any,
	options: Options
): {
	/**
	 * Decode a binary message
	 *
	 * @param {Uint8Array} message Message to decode
	 * @param {number} [length] The maximum number of bytes to consume. If
	 *   not set, will use message length
	 */
	decode: (
		message: Uint8Array,
		length?: number
	) => {
		decodedMessage: {};
		nextByte: number;
	};
};
export type ValueEnum = {
	/**
	 * Value for enum
	 */
	value: any;
	/**
	 * Label for enum value
	 */
	label: string;
	/**
	 * Key to use for enum value
	 */
	key: string;
};
export type BaseDataType = {
	/**
	 * Type of data
	 */
	type: string;
	/**
	 * Key of data
	 */
	id: string;
	/**
	 * Enum for data value
	 */
	enums: ValueEnum;
	/**
	 * If true, an Error will be
	 * thrown if the value doesn't match any of the enum values
	 */
	throwOnInvalidEnumValue?: boolean;
};
/**
 * Standard length integer
 */
export type IntegerDataAdditional = {
	type: 'byte' | 'uint8' | 'int8' | 'uint16' | 'int16' | 'uint32' | 'int32' | 'uint64' | 'int64';
	/**
	 * If true, integer will be decoded as
	 * littleEndian, otherwise bigEndian/networkByteOrder will be used
	 */
	littleEndian?: boolean;
};
export type IntegerDataType = BaseDataType & IntegerDataAdditional;
/**
 * Custom length integer
 */
export type BytesDataAdditional = {
	/**
	 * Number of bytes
	 */
	bytes: number;
	/**
	 * If true, the nextByte counter won't be
	 * incremented after decoding the values in this segment, meaning they
	 * will be processed by the next segment.
	 */
	partial?: boolean;
};
export type BytesDataType = BaseDataType & BytesDataAdditional;
export type BitPart = {
	/**
	 * ID for bit data. If not given, bits will be ignored
	 */
	id: string;
	/**
	 * Number of bits
	 */
	bits: number;
	/**
	 * Enum for data value
	 */
	enums: ValueEnum;
};
/**
 * Bit-sized binary data
 */
export type BitsDataAdditional = {
	type: 'bits';
	/**
	 * Bit-size binary data parts. Should total a multiple of
	 * bytes
	 */
	parts: BitPart;
	/**
	 * If true, the nextByte counter won't be
	 * incremented after decoding the values in this segment, meaning they
	 * will be processed by the next segment.
	 */
	partial?: boolean;
};
export type BitsDataType = BaseDataType & BitsDataAdditional;
/**
 * Integer spread over multiple parts
 */
export type BitsIntAdditional = {
	/**
	 * Bits to skip/take in alternating fashion.
	 * e.g. [0, 5, 3, 5, 3, 8] will be an 16-bit integer from 3 bytes,
	 * bits8:4 of the first byte, bits 8:4 of the second and the full last byte
	 */
	bits: Array<number>;
	/**
	 * If true, the nextByte counter won't be
	 * incremented after decoding the values in this segment, meaning they
	 * will be processed by the next segment.
	 */
	partial?: boolean;
};
/**
 * /**
 */
export type BitsIntType = BaseDataType & BitsIntAdditional;
/**
 * Variable length data. Will be stored as an Uint8Array
 */
export type VariableDataAdditional = {
	type: 'variable';
	/**
	 * Number of bytes of data or key
	 * to get the number of bytes from
	 */
	bytes: number | string;
};
export type VariableDataType = BaseDataType & VariableDataAdditional;
/**
 * String data
 */
export type StringDataAdditional = {
	type: 'string';
	/**
	 * The length of the string or the key
	 * to get the length of the string from
	 */
	length: number | string;
	/**
	 * If true, a NULL character (\0) will not
	 * terminate the string. Otherwise the string will be terminated and the
	 * rest of the string characters will be discarded
	 */
	noDiscard?: boolean;
};
export type StringDataType = BaseDataType & StringDataAdditional;
/**
 * Repeated data
 */
export type RepeatDataType = {
	type: 'repeat';
	/**
	 * Number of repeats or the key to get the
	 * number of repeats from
	 */
	repeats: number | string;
	/**
	 * If true, repeats will be stored in an array
	 */
	asArray?: boolean;
	/**
	 * If set, repeats will be stored in an object
	 * with the key for each repeat being the value of the key given in asObject
	 */
	asObject?: string;
	/**
	 * Structure of the repeated data
	 */
	structure: MessageStructure;
};
/**
 * Variable data type, using a previous value to determine the structure of
 * the data
 */
export type MapDataType = {
	type: 'map';
	/**
	 * Key to get map value from
	 */
	key: string;
	/**
	 * Map of data
	 * structures to use for data
	 */
	structures: {
		[key: string]: MessageStructure;
	};
};
/**
 * Structure configuration for the expected messages
 */
export type MessageStructure = Array<
	| IntegerDataType
	| BytesDataType
	| BitsDataType
	| BitsIntType
	| VariableDataType
	| StringDataType
	| RepeatDataType
	| MapDataType
>;
export type LoggerFn = (...args: any[]) => void;
export type Logger = {
	log?: LoggerFn;
	warn?: LoggerFn;
	error?: LoggerFn;
	info?: LoggerFn;
	debug?: LoggerFn;
};
/**
 * Decoder options
 */
export type Options = {
	/**
	 * If true, integers will be decoded as
	 * littleEndian, otherwise bigEndian/networkByteOrder will be used
	 */
	littleEndian?: boolean;
	/**
	 * If true, an Error will be
	 * thrown if the value doesn't match any of the enum values
	 */
	throwOnInvalidEnumValue?: boolean;
	/**
	 * Functions to use to log message during decoding
	 */
	logger?: Logger;
};
