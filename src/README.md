# binary-decoder <!=package.json version>

Decoder for structure binary data

[![pipeline status](https://gitlab.com/bytesnz/binary-decoder/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/binary-decoder/commits/main)
[![binary-decoder on NPM](https://bytes.nz/b/binary-decoder/npm)](https://npmjs.com/package/binary-decoder)
[![license](https://bytes.nz/b/binary-decoder/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/binary-decoder/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/binary-decoder/custom?color=yellowgreen&name=development+time&value=~5+hours)](https://gitlab.com/bytesnz/binary-decoder/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/binary-decoder/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/binary-decoder/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/binary-decoder/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Decoder module for decoding binary data that has a structured but variable
structure. The structure of the data is specified using an array of data parts.

## Data Types

### `byte`, `uint8`, `int8`, `uint16`, `int16`, `uint32`, `int32`, `uint63`, `int64`

An integer

#### Options

- **id** ID of field (will be used as the key in the output object)
- **littleEndian** If true, will decode as littleEndian
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed.
- **useBigInt** Always return a BigInt instead of a Number for integers. By
  default, if the number is 4 bytes or smaller, the value will be stored as
  a Number.

### `bytes`

An custom byte-length integer

#### Options

- **id** ID of field (will be used as the key in the output object)
- **littleEndian** If true, will decode as littleEndian
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed.
- **useBigInt** Always return a BigInt instead of a Number. By default, if
  the number is 4 bytes or smaller, the value will be stored as a Number.

### `bits`

Bit-size structured data. Must total a number of bytes.

#### Options

- **id** ID of field (will be used as the key in the output object). If not
  given, bit values will be set in containing object.
- **parts** Array of bit parts that make up the data
  - **id** ID of the bit part
  - **bit** The number of bits in the bit part
  - **enums** Enum of the bit value
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed. This is useful for using a map based
  on the first x-bits of a byte when the map includes the remaining bits in
  the byte

### `bitsInt`

Integer spread across multiple, separated bits

#### Options

- **id** ID of field (will be used as the key in the output object)
- **bits** An array of bits to skip/take in an alternating fashion e.g.
  [0, 5, 3, 5, 3, 8] will be an 16-bit integer from 3 bytes, bits8:4 of the
  first byte, bits 8:4 of the second and the full last byte
- **partial** If true, the nextByte counter that tracks the current position
  in the data will not be progressed. This is useful for using a map based
  on the first x-bits of a byte when the map includes the remaining bits in
  the byte
- **useBigInt** Always return a BigInt instead of a Number. By default, if
  the number is 4 bytes or smaller, the value will be stored as a Number.

### `variable`

Variable-length number

#### Options

- **id** ID of field (will be used as the key in the output object)
- **length** Length or key to value of length of data

### `string`

String. If a NULL character (`\0`) is encountered, the string will be
terminated and the rest of the bytes will be discarded unless the `noDiscard`
option is set.

#### Options

- **id** ID of field (will be used as the key in the output object)
- **length** Length or key to value of length of string

### `repeat`

n-parts of structured data

#### Options

- **id** ID of field (will be used as the key in the output object)
- **repeats** Number of parts
- **asArray** Store parts in an array
- **asObject** Store the parts in an object using the value for the given key
  as the part's key in the object

### `map`

Data that can be any of a given number of structures

#### Options

- **id** ID of field (will be used as the key in the output object)
- **key** Key to value to determine structure of data
- **structures** Object of value/structure of the possible data structures

## Example

```js
<!=!test/example.js>
```

### _structure.js_

```js
<!=!test/structure.js>
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
