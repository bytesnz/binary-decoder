module.exports = {
	root: true,
	extends: ['eslint:recommended', 'prettier'],
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020
	},
	ignorePatterns: ['.lintstagedrc.cjc', 'index.d.ts'],
	env: {
		browser: true,
		es2020: true,
		node: true
	},
	rules: {
		'no-fallthrough': 1,
		'no-case-declarations': 0,
		'no-console': [1, { allow: ['warn', 'error'] }],
		'no-debugger': 1,
		'no-warning-comments': [
			1,
			{ terms: ['xxx', 'todo', 'fixme', 'todo!!!'], location: 'anywhere' }
		],
		'require-jsdoc': 1
	},
	overrides: [
		{
			files: ['test/*.js'],
			rules: {
				'no-console': 0
			}
		}
	]
};
